/**
 * Function to return the response results.
 *
 * @param {Object} res The response Object.
 * @returns {Promise} A promise that resolves with the result of parsing the body text as JSON.
 */
export function formatDate(str) {
  const date = new Date(str);
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();
  const fullDate = `${month}/${day}/${year}`;

  return fullDate;
}

/**
 * Function to return the response results.
 *
 * @param {Object} res The response Object.
 * @returns {Promise} A promise that resolves with the result of parsing the body text as JSON.
 */
export function getData(req, opts, cb) {
  const api = 'https://api.themoviedb.org/3/';
  const apiKey = '9084eae9f770e006ebcba95dbd474e28';
  const type = opts.type;
  const queryVars = opts.queryVars;
  const endpoint = `${api}${type}?api_key=${apiKey}${queryVars}`;

  req.open('get', endpoint);
  req.onload = cb();
  req.send();
}

/**
 * Function to return the response results.
 *
 * @param {Object} res The response Object.
 * @returns {Promise} A promise that resolves with the result of parsing the body text as JSON.
 */
export function getResponse(res) {
  const results = res.json();

  return results;
}

/**
 * Function to return the error message.
 *
 * @param {Object} err The error object.
 * @returns {Object} The error message returned when data fetch fails.
 */
export function getError(err) {
  return err;
}
